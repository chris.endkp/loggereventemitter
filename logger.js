const EventEmitter = require('events');

var url = 'http://logger.io/log';

class Logger extends EventEmitter{
    log(message){
        //send HTTP request
        console.log(message);
        
        //rise an event
        this.emit('messageLogged', {id:1, logger_url:url, logger_message:message});
    }
}

module.exports = Logger;